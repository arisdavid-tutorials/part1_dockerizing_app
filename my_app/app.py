from flask import Flask


def create_app():
    """
    Create a Flask application

    :return: Flask app

    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config.settings')
    app.config.from_pyfile('settings.py', silent=True)

    @app.route("/")
    def index():
        return f"I am flask application. I'm running from {app.config['ENV_NAME']}."

    return app

app = create_app()

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True,
            host="0.0.0.0",
            port="5000")
