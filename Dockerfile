FROM python:3.6
MAINTAINER Aris David <contact@local.host>

ENV MY_APP_PATH /my_app
RUN mkdir -p $MY_APP_PATH

WORKDIR $MY_APP_PATH

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt


COPY . .

CMD gunicorn -b 0.0.0.0:5000 --access-logfile - "my_app.app:app"


